# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
#!/usr/bin/python3
# -*- coding: utf-8 -*-
import numpy as np

#This file displays a technique called Partition refinement
#https://en.wikipedia.org/wiki/Partition_refinement

class PartitionRefining():
    """
    .. py:class:: PartitionRefining

    Class display a technique representing a partition of a set as a data structure that allows the partition to be refined by splitting its sets into a larger number of smaller sets.
    From here onwards we denote by a family an elementary set, meaning that a so-called group must be the union of several families.
    """
    def __init__(self,cells_by_groupnumber,cells):
        """
        .. py:method:: __init__(cells_by_groupnumber,cells_by_family)

        Constructor of the class PartitionRefining

        :param int_by_int cells_by_groupnumber : dictionary expressing the association between each group and the cells belonging to them (in FE method either a set of nodes or a set of elements)
        :param numpy.unique array of int cells: set of all cells to be partitionned
        """
        self.cells_by_groupnumber=cells_by_groupnumber
        self.cells=cells
        self.groups_by_family=dict()
        self.cells_by_family=dict()

    def GetGroupsByFamily(self):
        """
        .. py:method:: GetGroupsByFamily()

        Retrieve association between groups and family
        """
        return self.groups_by_family

    def GetCellsByFamily(self):
        """
        .. py:method:: GetCellsByFamily()

        Retrieve association between groups and cells
        """
        return self.cells_by_family

    def ExecutePartitioning(self):
        """
        Implementation of the algorithm

        Provide a partition of the original set by updating it, cells_by_family, in a larger number of smaller disjoints sets (minimum required to describe the groups)
        Provide the association between each family and the groups that contain them by updating groups_by_family
        """
        #initialized to an empty group associated to family 1
        groups_by_family = {1 : []}
        #initialized to the trivial family 1 that contains every cells
        cells_by_family={1 : self.cells}
        # At each step of the algorithm, a set X is presented to the algorithm,
        for newgroup, groupcells in self.cells_by_groupnumber.items():
            split_by_family = {}

            for family, familycells in cells_by_family.items():
                intersect = np.intersect1d(familycells, groupcells, assume_unique=True)
                if len(intersect) == 0:
                    # family and group are disjoint, nothing to do for this family
                    continue
                # family intersects with this group
                if len(intersect) == len(familycells):
                    # family and group are coincident, this is the expected result
                    diff = None
                else:
                    # group is partially intersecting family, need to split this family
                    diff = np.setdiff1d(familycells, groupcells, assume_unique=True)
                split_by_family[family] = (intersect, diff)

            for family, split in split_by_family.items():
                intersect, diff = split
                if diff is not None:
                    cells_by_family[family] = intersect
                    newfamily = len(cells_by_family) + 1
                    cells_by_family[newfamily] = diff
                    groups_by_family[newfamily] = list(groups_by_family[family])
                groups_by_family[family].append(newgroup)

        self.groups_by_family=groups_by_family
        self.cells_by_family=cells_by_family

   


def CheckIntegrity():
    node_ids = range(1, 9 + 1)
    cells_by_groupnumber = {1:np.unique([1,2,3,4]), 2:np.unique([2,4,5]),3:np.unique([1,6]), 4:np.unique([7,8,9]), 5:np.unique([1,2,3,4,6])}
    PRef=PartitionRefining(cells_by_groupnumber,node_ids)
    PRef.ExecutePartitioning()

    groups_by_family,cells_by_family=PRef.GetGroupsByFamily(),PRef.GetCellsByFamily()

    print("cells_by_family: ",cells_by_family)
    print("groups_by_family: ",groups_by_family)

    from functools import reduce
    for family in groups_by_family.keys():
        group=tuple(cells_by_groupnumber[elem] for elem in groups_by_family[family])
        cells=reduce(np.intersect1d,group)
        cells.sort()
        ref=cells_by_family[family]
        if cells.all()!=ref.all():
            print("For family ",family," Cells obtained: ",cells," Cells expected: ",ref)
            raise Exception("The partition refining has failed")
    return "Ok"

if __name__ == '__main__':
    print(CheckIntegrity())# pragma: no cover
