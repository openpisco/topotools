# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

import numpy as np
from abc import ABCMeta

from Muscat.Helpers.TextFormatHelper import TFormat

class OptimProblemBase():
    __metaclass__ = ABCMeta

    def __init__(self, other = None, dataDeepCopy = True):
        super().__init__()

    def TakeValuesFrom(self,other):
        raise NotImplementedError("Must override method TakeValuesFrom")

##  the current point of the optim problem
    def GetCurrentPoint(self):
        raise NotImplementedError("Must override method GetCurrentPoint")

## the objective function val and Sensitivity df/dp
    def GetObjectiveFunctionName(self):
        raise NotImplementedError("Must override method GetObjectiveFunctionName")

    def GetObjectiveFunctionVal(self):
        raise NotImplementedError("Must override method GetObjectiveFunctionVal")

    def GetObjectiveFunctionSensitivity(self):
        raise NotImplementedError("Must override method GetObjectiveFunctionSensitivity")

## constraints number, val and sensistivity dC/dp
    def GetNumberOfInequalityConstraints(self):
        raise NotImplementedError("Must override method GetNumberOfInequalityConstraints")

    def GetNumberOfEqualityConstraints(self):
        raise NotImplementedError("Must override method GetNumberOfEqualityConstraints")

    def GetInequalityConstraintName(self,i):
        raise NotImplementedError("Must override method GetInequalityConstraintName")

    def GetEqualityConstraintName(self,i):
        raise NotImplementedError("Must override method GetEqualityConstraintName")

    def GetInequalityConstraintVal(self,i):
        raise NotImplementedError("Must override method GetInequalityConstraintVal")

    def GetEqualityConstraintVal(self,i):
        raise NotImplementedError("Must override method GetEqualityConstraintVal")

    def GetInequalityConstraintUpperBound(self,i):
        raise NotImplementedError("Must override method GetInequalityConstraintUpperBound")

    def GetInequalityConstraintSensitivityVal(self,i):
        raise NotImplementedError("Must override method GetInequalityConstraintSensitivityVal")

    def SetInequalityConstraintStatus(self,i,status):
        """ Inactive = 0,
            Automatic = 1, based on the Upperbound
        """
        raise NotImplementedError("Must override method SetInequalityConstraintStatus")

    def GetNumberOfActiveInequalityConstraints(self):
        return int(np.sum([ self.GetInequalityConstraintStatus(i) for i in range(self.GetNumberOfInequalityConstraints())  ]))

    def GetActiveInequalityConstraintsVals(self):
        res = []
        for i in range(self.GetNumberOfInequalityConstraints()):
            if self.GetInequalityConstraintStatus(i):
                res.append(self.GetInequalityConstraintVal(i))
        return res

    def GetActiveInequalityConstraintsUpperBound(self):
        res = []
        for i in range(self.GetNumberOfInequalityConstraints()):
            if self.GetInequalityConstraintStatus(i):
                res.append(self.GetInequalityConstraintUpperBound(i))
        return res

    def GetActiveInequalityConstraintsSensitivityVals(self):
        res = []
        for i in range(self.GetNumberOfInequalityConstraints()):
            if self.GetInequalityConstraintStatus(i):
                res.append(self.GetInequalityConstraintSensitivityVal(i))
        return res

    def GetActiveEqualityConstraintsVals(self):
        res=[]
        for i in range(self.GetNumberOfEqualityConstraints()):
            if self.GetEqualityConstraintStatus(i):
                res.append(self.GetEqualityConstraintVal(i))
        return res

    def GetActiveEqualityConstraintsTargetValues(self):
        res = []
        for i in range(self.GetNumberOfEqualityConstraints()):
            if self.GetEqualityConstraintStatus(i):
               res.append(self.GetEqualityConstraintTargetValue(i))
        return res

    def GetActiveEqualityConstraintsSensitivityVals(self):
        res = []
        for i in range(self.GetNumberOfEqualityConstraints()):
            if self.GetEqualityConstraintStatus(i):
                res.append(self.GetEqualityConstraintSensitivityVal(i))
        return res

    def GetInequalityConstraintStatus(self,i):
        raise NotImplementedError("Must override method GetInequalityConstraintStatus")

    def UpdateProblemWhenIterationAccepted(self):
        raise NotImplementedError("Must override method UpdateProblemWhenIterationAccepted")

## for pretty  output
    def GetNumberOfInternalVariables(self):
        return 1+self.GetNumberOfInequalityConstraints()+self.GetNumberOfEqualityConstraints()

    def GetVariableName(self,i):
        if i == 0:
            return self.GetObjectiveFunctionName()
        numberOfInequalityConstraints=self.GetNumberOfInequalityConstraints()
        if i <= numberOfInequalityConstraints:
           return self.GetInequalityConstraintName(i-1)
        return self.GetEqualityConstraintName(i-1-numberOfInequalityConstraints)

    def GetVariableValue(self,i):
        if i == 0: return self.GetObjectiveFunctionVal()
        numberOfInequalityConstraints=self.GetNumberOfInequalityConstraints()
        if i <= numberOfInequalityConstraints:
           return self.GetInequalityConstraintVal(i-1)
        return self.GetEqualityConstraintVal(i-1-numberOfInequalityConstraints)

    def PreStartCheck(self): return True

    def UpdateValues(self):
        raise NotImplementedError("Must override method UpdateValues")

    def Advance(self, direction, stepSize):
        """
        Parameters
        ----------
        direction : np.ndarray
            Vector-valued direction.

        stepSize : scalar
            A multiplicative factor for `direction`.

        Returns
        -------
        bool
            Whether the advance succeeded
        """
        self.point += stepSize * direction
        return True

    def GetDirectionFromGradient(self,gradient):
        return  -gradient

    def SaveData(self,data,point=None, gradient=None, direction=None):
        pass
    
    def PreStartCheck(self):
        return True 
    
################################


###### Output State ###############

    def PrintHeader(self):
        for i in range(self.GetNumberOfInequalityConstraints()):
            print(self.GetVariableName(i+1)+" Upper Bound : " + str(self.GetInequalityConstraintUpperBound(i) ))

        for i in range(self.GetNumberOfEqualityConstraints()):
            print(self.GetVariableName(self.GetNumberOfInequalityConstraints()+i+1)+" Target Value: " + str(self.GetEqualityConstraintTargetValue(i) ))

    def PrintStateHeader(self):
        res = ''
        for i in range(self.GetNumberOfInternalVariables()):
            res += '|'
            if i > 0 and i < self.GetNumberOfInequalityConstraints() :
                if self.GetInequalityConstraintStatus(i-1):
                    res += " "
                else:
                    res += "I"
            elif i > 0 and i > self.GetNumberOfInequalityConstraints():
                if self.GetEqualityConstraintStatus(i-1-self.GetNumberOfInequalityConstraints()):
                    res += " "
                else:
                    res += "I"
            else:
                res += " "
            res += TFormat.Center(self.GetVariableName(i), fill=" ", width=15)

        return res

    def PrintState(self):
        res = ""
        for i in range(self.GetNumberOfInternalVariables()):
            res += '|' + TFormat.Center('%.8e' % self.GetVariableValue(i), fill=" ", width=16)
        return res

    def PrintCurrentState(self, newPoint):


        def PrintOneState(oldval, newval, tocompare):
            res = ""
            if  tocompare == np.inf:
                if newval < oldval:
                    res += TFormat.InGreen(TFormat.Center( "%.8e"% newval,fill=" ",width=16) )+"|"
                else:
                    res += TFormat.InRed(TFormat.Center("%.8e"% newval,fill=" ",width=16) )+"|"
            else:
                if newval < tocompare:
                    res += TFormat.InGreen(TFormat.Center( "%.8e"% newval,fill=" ",width=16) )+"|"
                else:
                    res += TFormat.InRed(TFormat.Center("%.8e"% newval,fill=" ",width=16) )+"|"
            return res
        res = ""

        for i in range(self.GetNumberOfInternalVariables()):
            oldval = self.GetVariableValue(i)
            newval = newPoint.GetVariableValue(i)

            if i == 0:
                res += PrintOneState(oldval,newval, np.inf )
            elif i-1 < self.GetNumberOfInequalityConstraints():
                    # hack for
                res += PrintOneState(oldval,newval, newPoint.GetInequalityConstraintUpperBound(i-1) )
            else:
                equalityConstraintId=i-1-self.GetNumberOfInequalityConstraints()
                res += PrintOneState(oldval,newval, newPoint.GetEqualityConstraintTargetValue(equalityConstraintId) )

        return res

def CheckIntegrity():
    OptimProblemBase()
    return "OK"

if __name__ == '__main__':
    print(CheckIntegrity()) # pragma: no cover
