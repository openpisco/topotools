# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

import numpy as np

from OpenPisco.Optim.Problems.OptimProblemBase import OptimProblemBase
from OpenPisco.Optim.Problems.ProblemFactory import RegisterClass
from Muscat.Helpers.Logger import Debug

class OptimProblemDerived(OptimProblemBase):
    """
    Decorator base class for `OptimProblemBase`.

    Parameters
    ----------
    other : OptimProblemBase
        The underlying optimisation problem.

    dataDeepCopy : bool, optional
        Whether `other` must be cloned (default is True).

    Attributes
    ----------
    internalOptimProblem : OptimProblemBase
        The underlying optimisation problem.

    Notes
    -----
    This class implements a trivial decorator (An instance behaves exactly as
    the original object). Non-trivial decorators can be created subclassing
    `OptimProblemDerived` and overriding its methods.
    """
    def __init__(self, other=None, dataDeepCopy=True):
        super(OptimProblemDerived, self).__init__(other, dataDeepCopy)

        if other is None:
            self.internalOptimProblem = None
        else:
            self.internalOptimProblem = type(other.internalOptimProblem)(other.internalOptimProblem, dataDeepCopy)

    def SetInternalOptimProblem(self, of):
        self.internalOptimProblem = of

    def GetCurrentPoint(self):
        return self.internalOptimProblem.GetCurrentPoint()

    def GetObjectiveFunctionVal(self):
        return self.internalOptimProblem.GetObjectiveFunctionVal()

    def GetObjectiveFunctionSensitivity(self):
        return self.internalOptimProblem.GetObjectiveFunctionSensitivity()

    def GetNumberOfInequalityConstraints(self):
        return self.internalOptimProblem.GetNumberOfInequalityConstraints()

    def GetNumberOfEqualityConstraints(self):
        return self.internalOptimProblem.GetNumberOfEqualityConstraints()

    def GetInequalityConstraintSlacks(self):
        return self.internalOptimProblem.GetInequalityConstraintSlacks()

    def GetInequalityConstraintName(self,i):
        return self.internalOptimProblem.GetInequalityConstraintName(i)

    def GetInequalityConstraintStatus(self,i):
        return self.internalOptimProblem.GetInequalityConstraintStatus(i)

    def GetInequalityConstraintVal(self, i):
        return self.internalOptimProblem.GetInequalityConstraintVal(i)

    def GetInequalityConstraintValOriginal(self, i):
        return self.internalOptimProblem.GetInequalityConstraintValOriginal(i)

    def GetInequalityConstraintUpperBoundOriginal(self,i):
        return self.internalOptimProblem.GetInequalityConstraintUpperBoundOriginal(i)

    def GetInequalityConstraintUpperBound(self,i):
        return self.internalOptimProblem.GetInequalityConstraintUpperBound(i)

    def UpdateValues(self):
        return self.internalOptimProblem.UpdateValues()

    def PrintHeader(self):
        self.internalOptimProblem.PrintHeader()

    def PrintStateHeader(self):
        return self.internalOptimProblem.PrintStateHeader()

    def PrintState(self):
        return self.internalOptimProblem.PrintState()

    def PrintCurrentState(self, newPoint):
        return self.internalOptimProblem.PrintCurrentState(newPoint.internalOptimProblem)

    def Advance(self, direction, stepSize):
        return self.internalOptimProblem.Advance(direction, stepSize)

    def SaveData(self,data,point=None, gradient= None, direction = None ):
        self.internalOptimProblem.SaveData(data, point=point, gradient=gradient, direction=direction)

    def GetDirectionFromGradient(self,grad):
        return self.internalOptimProblem.GetDirectionFromGradient(grad)

    def TakeValuesFrom(self,other):
        self.internalOptimProblem.TakeValuesFrom(other.internalOptimProblem)

    def UpdateProblemWhenIterationAccepted(self):
        self.internalOptimProblem.UpdateProblemWhenIterationAccepted()


class OptimProblemPenalized(OptimProblemDerived):

    def __init__(self,other = None,dataDeepCopy=True):
        super(OptimProblemPenalized,self).__init__(other,dataDeepCopy)
        if other is None:
            self.penal = {}
        else:
            self.penal = other.penal.copy()

    def SetPenals(self, dic):
        for k,v in dic.items():
            self.SetPenal(k,v)

    def SetPenal(self, name, penal):
        """Set the value of the penalty factor.

        Parameters
        ----------
        penal : scalar or iterable of scalars
            Value of the penalty factor.

        nb : int, optional
            The index of the constraint for which a penalty factor is set. If
            `None`, the same penalty factor is used for all the constraints
            when `penal` is a scalar, or each constraint has its own penalty
            factor when `penal` is iterable.
        """
        self.penal[name] = float(penal)
        Debug("SetPenal called '" + str(name) + "' = " +str(float(penal)))

    def GetPenal(self,name):
        return self.penal[name]

    def GetInequalityConstraintVal(self, i):
        return self.penal[self.internalOptimProblem.GetInequalityConstraintName(i)] * self.internalOptimProblem.GetInequalityConstraintVal(i)

    def GetInequalityConstraintSensitivityVal(self,i):
        return self.penal[self.internalOptimProblem.GetInequalityConstraintName(i)] * self.internalOptimProblem.GetInequalityConstraintSensitivityVal(i)

    def PrintHeader(self):
        print(" penal : " + str(self.penal))
        super(OptimProblemPenalized,self).PrintHeader()

    def UpdateValues(self):
        if self.GetNumberOfInequalityConstraints() and len(self.penal) == 0 :
            raise ValueError('Must call SetPenal first')
        return super(OptimProblemPenalized,self).UpdateValues()

    def TakeValuesFrom(self,other):
        super(OptimProblemPenalized,self).TakeValuesFrom(other)
        self.penal = other.penal

RegisterClass("Penalized", OptimProblemPenalized)


class OptimProblemSquared(OptimProblemDerived):
    """Replaces an optimization problem by one where constraint violation are
    squared.
    """

    def __init__(self, other=None, dataDeepCopy=True):
        super(OptimProblemSquared, self).__init__(other, dataDeepCopy)

    def UpdateValues(self):
        return  super(OptimProblemSquared, self).UpdateValues()

    def GetInequalityConstraintVal(self, i):
        return self._slackValue(i)**2

    def GetInequalityConstraintSensitivityVal(self, i):
        return (2 * self._slackValue(i)) * self.internalOptimProblem.GetInequalityConstraintSensitivityVal(i)

    def _slackValue(self, i):
        diff = self.internalOptimProblem.GetInequalityConstraintVal(i) - self.internalOptimProblem.GetInequalityConstraintUpperBound(i)
        return np.maximum(diff, 0.)

    def GetInequalityConstraintUpperBound(self,i):
        return 0.0

RegisterClass("Squared", OptimProblemSquared)


class OptimProblemPenaltyL1(OptimProblemPenalized):
    """L1 penalty function"""

    def __init__(self,other = None,dataDeepCopy=True):
        super(OptimProblemPenaltyL1,self).__init__(other,dataDeepCopy)

    def GetInequalityConstraintVal(self,i ):
        slack = self.internalOptimProblem.GetInequalityConstraintVal(i) - self.internalOptimProblem.GetInequalityConstraintUpperBound(i)
        slack = max(0,slack)
        return self.penal[self.internalOptimProblem.GetInequalityConstraintName(i)]*slack

    def GetInequalityConstraintSensitivityVal(self,i):
        slack = self.internalOptimProblem.GetInequalityConstraintVal(i) - self.internalOptimProblem.GetInequalityConstraintUpperBound(i)
        if slack <= 0:
            return 0*self.internalOptimProblem.GetInequalityConstraintSensitivityVal(i)
        else:
            return self.penal[i]*self.internalOptimProblem.GetInequalityConstraintSensitivityVal(i)

    def PrintHeader(self):
        print(" penal : " +str(self.penal))
        super(OptimProblemPenaltyL1,self).PrintHeader()

    def UpdateValues(self):
        assert self.penal is not None,'Must call SetPenal first'
        return  super(OptimProblemPenaltyL1,self).UpdateValues()

    def TakeValuesFrom(self,other):
        super(OptimProblemPenaltyL1,self).TakeValuesFrom(other)
        self.penal = other.penal

class OptimProblemAddAllContributions(OptimProblemDerived):
    def __init__(self,other = None,dataDeepCopy=True):
        super(OptimProblemAddAllContributions,self).__init__(other,dataDeepCopy)
    def GetNumberOfInequalityConstraints(self):
        return 0
    def GetInequalityConstraintVal(self,i):
        raise NotImplementedError()
    def GetInequalityConstraintSensitivityVal(self,i):
        raise NotImplementedError()

    def GetObjectiveFunctionVal(self):
        objective =  self.internalOptimProblem.GetObjectiveFunctionVal()

        for i in range(self.internalOptimProblem.GetNumberOfInequalityConstraints()):
            if self.internalOptimProblem.GetInequalityConstraintStatus(i) == 0 :
                continue
            objective += self.internalOptimProblem.GetInequalityConstraintVal(i)
        return objective

    def GetObjectiveFunctionSensitivity(self):
        gradient = self.internalOptimProblem.GetObjectiveFunctionSensitivity().flatten()
        for i in range(self.internalOptimProblem.GetNumberOfInequalityConstraints()):
            if self.internalOptimProblem.GetInequalityConstraintStatus(i) == 0 :
                continue
            gradient += self.internalOptimProblem.GetInequalityConstraintSensitivityVal(i).flatten()

        return gradient

def CheckIntegrity():
    OptimProblemDerived()
    return "OK"

if __name__ == '__main__':
    print(CheckIntegrity()) # pragma: no cover
