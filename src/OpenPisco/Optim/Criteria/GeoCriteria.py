# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

import numpy as np

import Muscat.Helpers.ParserHelper as PH
from Muscat.Containers.MeshInspectionTools import ExtractElementByTags, ExtractElementsByElementFilter
from Muscat.Containers.MeshInspectionTools import GetVolume
from Muscat.Containers.Filters.FilterObjects import ElementFilter as ElementFilter
from Muscat.Containers.Filters.FilterOperators import DifferenceFilter
from Muscat.Containers.MeshModificationTools import CleanLonelyNodes
from Muscat.ImplicitGeometry.ImplicitGeometryTools import DistanceToSurface
from Muscat.Types import MuscatFloat

from OpenPisco.Optim.Criteria.CriteriaFactory import RegisterClass as RegisterCriteriaClass
from OpenPisco.Optim.Criteria.Criteria import CriteriaBase
import OpenPisco.TopoZones as TZ

from OpenPisco import RETURN_SUCCESS

class TopoCriteriaVolume(CriteriaBase):
     def __init__(self, other=None):
        super(TopoCriteriaVolume, self).__init__(other)

        self.volume = 0.0
        self.npoints = 0
        self.tagsToKeep = [""]
        if other is None:
            self.SetName("Volume")
            self.dVolumedOmega = np.array(0)
            self.point = None
        else:
            self.volume = other.volume
            self.npoints = other.npoints
            self.point = other.point
            self.dVolumedOmega = other.dVolumedOmega.copy()

     def GetNumberOfSolutions(self):
         return 0

     def UpdateValuesUsingPhi(self,levelSet,phi):
         from OpenPisco.Unstructured.Levelset import LevelSet
         ls = LevelSet()
         ls.conform = False
         ls.phi = phi
         ls.support = levelSet.support
         return self.UpdateValues(ls)

     def UpdateValues(self,levelSet):
         self.npoints = levelSet.phi.size
         #if the mesh changes then we recompute the mass matrix
         if self.point is not None:
             if levelSet.support is not self.point.support:
                 self.M = None

         self.point = levelSet

         if levelSet.conform :
            tokeep = [ TZ.Inside3D ]
            tokeep.extend(self.tagsToKeep)
            self.cleanmesh = ExtractElementByTags(levelSet.support,tokeep,cleanLonelyNodes=False)
            self.volume = GetVolume(self.cleanmesh)
         else:
            self.volume = levelSet.GetVolumeOfNegativePartbyElement(levelSet.phi).sum()

         return RETURN_SUCCESS

     def GetValue(self):
         return self.volume

     def GetSensitivity(self):
        return  np.ones(self.npoints)

RegisterCriteriaClass("Volume", TopoCriteriaVolume)

class TopoCriteriaMass(TopoCriteriaVolume):
     def __init__(self, other=None):
         super(TopoCriteriaMass, self).__init__(other)

         if other is None:
            self.density = 1.
            self.SetName("Mass")
         else:
            self.density = other.density

     def GetValue(self):
         return super(TopoCriteriaMass, self).GetValue()*self.density

     def GetSensitivity(self):
        return super(TopoCriteriaMass, self).GetSensitivity()*self.density

RegisterCriteriaClass("Mass", TopoCriteriaMass)

class TopoCriteriaContactSurfacePenalization(CriteriaBase):
    def __init__(self, other=None):
        super(TopoCriteriaContactSurfacePenalization, self).__init__(other)
        if other is None:
           self.SetName("ContactSurfacePenalization")
           self.eTags = np.zeros(0,dtype=str)
           self.length = 1e-3
           self.overhangTag = None
           self.surfMesh = None
           self.dist = None
        else:
           self.eTags = PH.ReadStrings(other.eTags)
           self.length = other.length
           self.overhangTag = other.overhangTag
           self.dist = other.dist

    def GetNumberOfSolutions(self):
        return 1

    def GetSolution(self,i):
        self.AssertSuitableSolutionId(i)
        return self.contactarea

    def GetSolutionName(self,i):
        self.AssertSuitableSolutionId(i)
        return "ContactArea"

    def ComputeDistanceSurface(self,levelSet):
        fil = ElementFilter(dimensionality=[levelSet.support.GetElementsDimensionality()-1],eTag=self.eTags)
        if self.overhangTag is not None:
            oTfil = ElementFilter(dimensionality=[levelSet.support.GetElementsDimensionality()-1],eTag=self.overhangTag)

            fil = DifferenceFilter(filters=[fil,oTfil])

        surfMesh = ExtractElementsByElementFilter(levelSet.support, fil, copy=False)


        CleanLonelyNodes(surfMesh)
        surfMesh.PrepareForOutput()
        self.dist = DistanceToSurface(levelSet.support, surfMesh )

    def UpdateValues(self,levelSet):

        self.fSensitivity_val = np.zeros(levelSet.support.GetNumberOfNodes() ,dtype=MuscatFloat)
        nodeMask = np.zeros(levelSet.support.GetNumberOfNodes(),dtype=bool)

        if self.dist is None:
           self.ComputeDistanceSurface(levelSet)

        nodeMask[self.dist<self.length] = True
        nodeMask[levelSet.phi >= self.length] = False

        self.contactarea = np.abs(self.dist - self.length)
        self.contactarea[nodeMask] = -self.dist[nodeMask]

        self.f_val = levelSet.GetVolumeOfNegativePartbyElement(self.contactarea).sum()/(self.length)
        self.fSensitivity_val[nodeMask] = 1./(self.length)

        return RETURN_SUCCESS

    def GetValue(self):
        return self.f_val

    def GetSensitivity(self):
        return  self.fSensitivity_val

RegisterCriteriaClass("ContactSurfacePenalization", TopoCriteriaContactSurfacePenalization)

def CheckIntegrity(GUI=False):
    c=TopoCriteriaVolume()

    from OpenPisco.Structured.Levelset3D  import LevelSet3D
    from Muscat.ImplicitGeometry.ImplicitGeometryObjects import ImplicitGeometrySphere
    from Muscat.IO.XdmfWriter import XdmfWriter
    from Muscat.Helpers.IO.TemporaryDirectory import TemporaryDirectory
    from Muscat.Containers.ConstantRectilinearMeshTools import CreateConstantRectilinearMesh

    writer = XdmfWriter(TemporaryDirectory.GetTempPath()+'Test_GeoCriteria.xmf')
    writer.SetTemporal()
    writer.SetBinary()
    writer.SetXmlSizeLimit(0)
    writer.Open()

    n = 21
    print(n)
    myMesh = CreateConstantRectilinearMesh(dimensions=[n,n,n],spacing=[20./(n-1), 20./(n-1), 20./(n-1)], origin=[-10.,-10.,-10] )
    ls = LevelSet3D(support=myMesh)

    r = 7.
    sp = ImplicitGeometrySphere()
    sp.SetCenter([0.,0.,0.])
    sp.SetRadius(r)
    ls.Initialize(sp )
    c.UpdateValues(ls)

    #theorical volume, surface
    tv = 4./3.*np.pi*r**3
    ts = 4.*np.pi*r**2
    volume = c.GetValue()
    print("Volume : {0:.3f},  {1:.3f}, error: {2:.3f}%".format(tv,volume,(tv-volume)/tv*100) )

    one = np.ones(len(ls.phi))
    dvdphi = c.GetSensitivity()
    surface =  ls.InterfaceIntegral(one)
    print("surface  : {0:.3f},  {1:.3f}, error: {2:.3f}% ".format(ts,surface,(ts-surface)/ts*100) )
    from OpenPisco.Structured.Laplacian3D import ConstantMassMatrix
    M = ConstantMassMatrix(myMesh)
    dirac = ls.InterfaceRestriction(ls.phi).flatten()
    surface2 = np.dot(dvdphi,M*dirac)
    print("surfac 2 : {0}".format(surface2) )
    surface2 = np.dot(dvdphi,np.sum(M,axis=1))
    print("surfac 2 : {0}".format(surface2))

    speed = ls.Regularize(dvdphi,lengthscaleParameter=1)
    writer.Write(myMesh,
            PointFields     = [ls.phi,sp(myMesh.nodes),dirac,dvdphi,speed],
            PointFieldsNames= ['phi','phiExact',"dirac",'dvdphi',"speed"],
            TimeStep = 1
            )

    print("After transport")

    dt = 0.24
    tv = 4./3.*np.pi*(r+dt)**3
    ts = 4.*np.pi*(r+dt)**2

    tvOrdre1 = 4./3.*np.pi*(r)**3 + dt * (4.*np.pi*(r)**2)
    tsOrdre1 = 4.*np.pi*(r)**2 + dt*(8*np.pi*r)
    ff = ls.phi.ravel() - dt * (speed.ravel())

    tvOrdre1Numeric  = ls.GetVolumeOfNegativePartbyElement(ff).sum()
    print("tvOrdre1Numeric  : ", tvOrdre1Numeric)
    tvOrdre1Numberic2 = tv+ls.InterfaceIntegral(speed)*dt
    print("tvOrdre1Numberic2 : ", tvOrdre1Numberic2)

    ls.TransportAndReinitialize(speed,dt)
    c.UpdateValues(ls)

    volume = c.GetValue()
    dvdphi = c.GetSensitivity()
    speed = ls.Regularize(dvdphi,lengthscaleParameter=1.)

    surface =  ls.InterfaceIntegral(one)

    print("volume : {0:.3f}, {1:.3f}, {2:.3f}".format(tv,tvOrdre1,volume))
    print("vError : {0}%, {1:.3f}%, {2:.3f}%".format(0.000,(tv-tvOrdre1)/tv*100,(tv-volume)/tv*100))
    print("surface  : {0:.3f}, {1:.3f}, {2:.3f} ".format(ts,tsOrdre1,surface))
    print("surface  : {0:.3f}, error:  {1:.3f}, {2:.3f}% ".format(ts,(ts-tsOrdre1)/ts*100,(ts-surface)/ts*100))

    sp.SetRadius(r+dt)

    dirac = ls.InterfaceRestriction(ls.phi).flatten()
    writer.Write(myMesh,
            PointFields     = [ls.phi,sp(myMesh.nodes),dirac,dvdphi,speed],
            PointFieldsNames= ['phi','phiExact','dirac','dvdphi',"speed"],
            TimeStep = 1
            )
    writer.Close()

    return "OK"

if __name__ == "__main__":# pragma: no cover
    print(CheckIntegrity())
