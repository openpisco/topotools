# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

class ActionBase():
    needToCancel = False

    @classmethod
    def SetNeedToInterrupt(cls):
        cls.needToCancel = True

    @classmethod
    def NeedToInterrupt(cls,keepState = False):
        if cls.needToCancel:
            if not keepState :
                cls.needToCancel = False
            return True
        else:
            return False

    def __init__(self):
       super(ActionBase, self).__init__()

    def Apply(self, app, ops):
        raise NotImplementedError

def CheckIntegrity():
    ActionBase()
    return "ok"

if __name__ == '__main__':
        print(CheckIntegrity())
