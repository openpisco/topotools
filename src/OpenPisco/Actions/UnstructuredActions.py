# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

# -*- coding: utf-8 -*-

import numpy as np

import Muscat.Helpers.ParserHelper as PH
from Muscat.Containers.Filters.FilterObjects import ElementFilter, NodeFilter
from Muscat.Containers.Filters.FilterOperators import ComplementaryFilter, DifferenceFilter, IntersectionFilter, UnionFilter
from Muscat.Containers.MeshModificationTools import AddTagPerBody
from Muscat.Containers.MeshInspectionTools import ExtractElementsByElementFilter,CleanLonelyNodes,ExtractElementByTags,GetVolume,GetMeasure
from Muscat.Helpers.Logger import Info, Debug, Error

from OpenPisco.Actions.ActionFactory import RegisterClass
from OpenPisco.Actions.ActionBase import ActionBase
from OpenPisco import RETURN_SUCCESS, RETURN_FAIL

class CreateTags(ActionBase):
    def __init__(self):
        super(CreateTags,self).__init__()

    def Apply(self, app, ops):
        ls = ops['ls']
        del ops['ls']

        optionsToRead = list(ops.keys())
        optionsToRead.remove("type")
        options = {'nodalprefix' : "NT",
                   'nodalTags' : np.empty(0,dtype=int),
                   'elemenprefix' :"ET",
                   'elementTags' : np.empty(0,dtype=int),
                   'dim': ls.support.GetElementsDimensionality()}

        PH.ReadProperties(ops,optionsToRead,options)

        for tag in options['nodalTags']:
            val = app.zones[tag](ls.support)
            w = np.nonzero(val<=0)[0]
            ls.support.nodesTags.CreateTag(options["nodalprefix"]+str(tag)).SetIds(w)

        dim = options['dim']

        from Muscat.Containers.Filters.FilterObjects import ElementFilter

        for tag in options['elementTags']:
            ff = ElementFilter()
            zone = app.zones[tag]
            ff.AddZone(zone)
            ff.SetDimensionality(dim)
            for name,data,ids in ff(ls.support):
                data.tags.CreateTag(options["elemenprefix"]+str(tag)).SetIds(ids)
                if not len(ids):
                   Info("Warning : creating empty tag "+options["elemenprefix"]+str(tag)+" for elements of type "+name)

        return RETURN_SUCCESS

RegisterClass("CreateTagsFromZones",CreateTags)

class CreateElementTag(ActionBase):
    def __init__(self):
        super(CreateElementTag,self).__init__()

    def Apply(self, app, ops):

        ls = ops['ls']
        del ops['ls']

        FilterOperatorOps = {'intersection'  : IntersectionFilter,
                             'difference'    : DifferenceFilter,
                             'union'         : UnionFilter,
                             'complementary' : ComplementaryFilter }

        filters = []
        for tagname in PH.ReadStrings(ops["elementTags"]):
            filters.append( ElementFilter(eTag=tagname), )

        operatortype = ops.get('operator','intersection')
        FilterOperator = FilterOperatorOps[operatortype](filters)

        for name,data,ids in FilterOperator(ls.support):
            data.tags.CreateTag(ops["tagname"]).SetIds(ids)
            if not len(ids):
               Info("Warning : creating empty tag "+str(ops["tagname"])+" for elements of type "+name)

        return RETURN_SUCCESS

RegisterClass("CreateElementTag", CreateElementTag)

class CreateNodeTag(ActionBase):
    def __init__(self):
        super(CreateNodeTag,self).__init__()

    def Apply(self, app, ops):

        ls = ops['ls']
        del ops['ls']

        FilterOperatorOps = {'intersection'  : IntersectionFilter,
                             'difference'    : DifferenceFilter,
                             'union'         : UnionFilter,
                             'complementary' : ComplementaryFilter }
        filters = []
        for tagname in PH.ReadStrings(ops["nodeTags"]):
            filters.append( NodeFilter(nTag=tagname), )
        operatortype = ops.get('operator','intersection')
        FilterOperator = FilterOperatorOps[operatortype](filters)
        ids=FilterOperator.GetNodesIndices(ls.support) 
        ls.support.nodesTags.CreateTag(ops["tagname"]).SetIds(ids)
        if not len(ids):
            Info("Warning : creating empty node tag "+str(ops["tagname"]))

        return RETURN_SUCCESS

RegisterClass("CreateNodeTag", CreateNodeTag)

class CreateZonesFromETags(ActionBase):
    """
    Create zones from etag:
         ls: level-set
         etags: existing etag name
         zones: zone ids
    """
    def __init__(self):
        super().__init__()

    def Apply(self, app, ops):
        ls = ops['ls']
        del ops['ls']

        tagnames = PH.ReadString(ops['etags']).split(" ")
        zones = PH.ReadInts(ops["zones"])
        assert len(tagnames) == len(zones),"Number of zones and tagnames provided not consistent"

        if any(zone in app.zones.keys() for zone in zones):
          existingZone=np.fromiter(app.zones.keys(), dtype=int)
          Info("Existing zone IDs: "+str(existingZone))
          Info("To be created zone IDs: "+str(zones))
          Info("Common zone IDs: "+str(np.intersect1d(existingZone, zones)))
          raise Exception("At least one existing zone will be overwritten, not allowed")

        from Muscat.ImplicitGeometry.ImplicitGeometryObjects import CreateImplicitGeometryByETag
        for tagname,zone in zip(tagnames,zones):
            IGByETag = CreateImplicitGeometryByETag({"support":ls.support, "eTags":[tagname]})
            app.zones[zone]=IGByETag

        return RETURN_SUCCESS

RegisterClass("CreateZonesFromETags",CreateZonesFromETags)


class CreateTagsFromElemField(ActionBase):
    def __init__(self):
        super(CreateTagsFromElemField,self).__init__()

    def Apply(self, app, ops):

        ls = ops['ls']

        fieldname = ops.get('fieldname' ,'Label')
        prefix = ops.get('prefix' ,'eTag')
        field = ls.support.elemFields[fieldname]
        tagnumbers = np.unique(field)

        for _,data in ls.support.elements.items():
            for integer in tagnumbers:
                elemfield = field[data.globaloffset:data.globaloffset+data.GetNumberOfElements()]
                ids = np.nonzero(elemfield ==integer )[0]
                if len(ids):
                   data.GetTag(prefix+str(integer)).SetIds(ids)

        return RETURN_SUCCESS

RegisterClass("CreateTagsFromElemField",CreateTagsFromElemField)


class AddETagFromNTag(ActionBase):
    def __init__(self):
        super(AddETagFromNTag,self).__init__()

    def Apply(self,app, ops):

        ls = ops['ls']
        nodaltagNames = PH.ReadStrings(ops.get('ntagNames'))
        etagNames= PH.ReadStrings(ops.get('etagNames',[]))

        if len(etagNames) == 0:
            etagNames = [""]*len(nodaltagNames)
        elif len(etagNames) != len(nodaltagNames):
            raise(Exception("The number of nodaltagNames and the etagNames must be the same"))

        elementprefix = ops.get("elemenprefix","ET")

        from Muscat.Containers.Filters.FilterObjects import ElementFilter
        ef = ElementFilter()

        if 'dim' in ops:
            ef.SetDimensionality(PH.ReadInt(ops['dim']) )

        nodalMask = np.zeros(ls.support.GetNumberOfNodes(),dtype = bool)
        for etagName,name in zip(etagNames,nodaltagNames):
            nodalMask.fill(False)
            tag = ls.support.GetNodalTag(name)
            nodalMask[tag.GetIds()] = True
            for name, data, ids in ef(ls.support):
                vals = np.sum(nodalMask[data.connectivity],axis=0)
                idd = np.nonzero( vals == data.GetNumberOfPointsPerElement())[0]
                if len(idd):
                    data.GetTag(elementprefix+str(etagName)).AddToTag(idd)

        return RETURN_SUCCESS

RegisterClass("AddETagFromNTag",AddETagFromNTag)

class AddNTagFromETag(ActionBase):
    """
    Add nodal tags from element tags:
        ls: "levelset"
        etagNames:strings
        ntagNames:strings
    """
    def __init__(self):
        super(AddNTagFromETag,self).__init__()

    def Apply(self,app, ops):

        ls = ops['ls']
        etagNames=PH.ReadStrings(ops.get('etagNames'))
        nodaltagNames=PH.ReadStrings(ops.get('ntagNames',[]))

        assert len(etagNames) == len(nodaltagNames),"The number of nodaltagNames and the etagNames must be the same"
        for etagName,ntagName in zip(etagNames,nodaltagNames):
            for _, data in ls.support.elements.items():
                if etagName in data.tags:
                    nodalids = data.GetNodesIdFor(data.tags[etagName].GetIds())
                    if len(nodalids):
                       ls.support.GetNodalTag(ntagName).AddToTag(nodalids)

        return RETURN_SUCCESS

RegisterClass("AddNTagFromETag",AddNTagFromETag)


class AddNodalTagUsingOriginalId(ActionBase):
    def __init__(self):
        super(AddNodalTagUsingOriginalId,self).__init__()

    def Apply(self, app, ops):
        ls = ops['ls']
        del ops['ls']

        optionsToRead = list(ops.keys())
        optionsToRead.remove("type")
        options = {'name' : "",
                   'ids' : np.empty(0,dtype=int),
                   'tag' : ""}

        PH.ReadProperties(ops,optionsToRead,options)

        assert options["name"] != "","The name '"+options["name"]+"' is not valid"

        if options['tag'] != "":
            ls.support.nodesTags.CreateTag(options['name'],False).AddToTag(ls.support.nodesTags[options['tag']].GetIds())

        for ID in options['ids']:
            ls.support.AddNodeToTagUsingOriginalId(ID,options['name'])

        return RETURN_SUCCESS

RegisterClass("AddNodalTag", AddNodalTagUsingOriginalId)


class ActionCutMeshIsoZero(ActionBase):
    def __init__(self):
        super(ActionCutMeshIsoZero,self).__init__()

    def Apply(self, app, ops):
        ls = ops['ls']
        if ls.support.props.get("IsConstantRectilinear",False):
            Error("Cant remesh this type of mesh")
            return RETURN_FAIL

        from Muscat.ImplicitGeometry.ImplicitGeometryTools import CutMeshIsoZero
        import OpenPisco.TopoZones as TZ
        ls.support = CutMeshIsoZero(ls.support, ls.phi ) [0]

        for _,data in ls.support.elements.items():
            data.tags.RenameTag("isoNegative",TZ.Inside3D,noError=True)
            data.tags.RenameTag("isoPositive",TZ.Outside3D,noError=True)
            data.tags.RenameTag("isoZero",TZ.InterSurf,noError=True)

        import Muscat.Containers.MeshInspectionTools as UMIT

        surf = UMIT.ExtractElementByTags(ls.support, [TZ.InterSurf])

        from Muscat.ImplicitGeometry.ImplicitGeometryObjects import ImplicitGeometryStl

        print("computing distance ")
        op = ImplicitGeometryStl()
        op.SetSurface(surf)
        newphi = abs(op(ls.support.nodes))
        interior = UMIT.ExtractElementByTags(ls.support, [TZ.Inside3D])
        newphi[interior.originalIDNodes] *= -1
        print("end")
        ls.phi = newphi

        return RETURN_SUCCESS

RegisterClass("CutMeshIsoZero",ActionCutMeshIsoZero)


class ActionRemesh(ActionBase):
    def __init__(self):
       super(ActionRemesh,self).__init__()

    def Apply(self, app, ops):
        ls = ops['ls']
        if ls.support.props.get("IsConstantRectilinear",False):
            Error("Cant remesh this type of mesh")
            return RETURN_FAIL

        force = PH.ReadBool(ops.get("force",False))

        if not ls.conform and force == False:
            Error("Cant remesh levelset, not conform")
            return RETURN_FAIL

        import  OpenPisco.Unstructured.MmgMesher as MmgMesher
        #copy specs form the levelset
        remeshOptions = ls.MesherInfo.copy()
        # we delete the iso becase the user just want to do a remesh with no iso
        remeshOptions.pop("iso",None)
        # push action options and override defaut if necesary
        remeshOptions.update(ops)
        for key in ("hmin", "hmax", "hausd", "hgrad","hsiz","iso","ar"):
            if key in ops:
               remeshOptions[key] = PH.ReadFloat(ops[key])

        for key in ("rmc","met","keepGeneratedFiles"):
            if key in ops:
               remeshOptions[key] = PH.ReadBool(ops[key])
        if "met" in remeshOptions.keys() and remeshOptions["met"]:
           from OpenPisco.Unstructured.MeshAdaptationTools import ComputeLevelSetMetric
           metricfield = ComputeLevelSetMetric(ls,remeshOptions)
           ls.support.nodeFields["met"] = metricfield

        MmgMesher.MmgMesherActionLevelset(ls,remeshOptions)

        return RETURN_SUCCESS

RegisterClass("Remesh", ActionRemesh)

class ActionCleanLonelyNodes(ActionBase):
    def __init__(self):
        super(ActionCleanLonelyNodes,self).__init__()

    def Apply(self, app, ops):
        ls = ops['ls']

        if not ls.support.props.get("IsConstantRectilinear",False):
            print("Cant CleanLonelyNodes on this type of mesh")
            return 0

        CleanLonelyNodes(ls.support)
        ls.phi = ls.phi[ls.support.originalIDNodes ]
        return RETURN_SUCCESS

RegisterClass("CleanLonelyNodes", ActionCleanLonelyNodes)

class ActionCreate0Delement(ActionBase):
    def __init__(self):
        super(ActionCreate0Delement,self).__init__()

    def Apply(self, app, ops):
        ls = ops['ls']

        if not ls.support.props.get("IsConstantRectilinear",False):
            print("Cant call ActionCreate0Delement on this type of mesh")
            return 0

        import Muscat.Containers.ElementsDescription as ED
        elements = ls.support.GetElementsOfType(ED.Point_1)

        zone = ops.get('zone',None)

        if zone is not None:
            val = zone(ls.support)
            w = np.nonzero(val<=0)[0]
            for idd in w:
                elements.AddNewElement([idd],0)

        nTag = ops.get('nTag',None)
        if nTag is not None:
            val = zone(ls.support)
            w = ls.support.GetNodalTag(nTag).GetIds()
            tag = elements.CreateTag(nTag)
            for idd in w:
                cpt = elements.AddNewElement([idd],0)
                tag.AddId(cpt-1)

        ls.support.PrepareForOutput()
        return RETURN_SUCCESS

RegisterClass("Create0DElements", ActionCreate0Delement)

class FilterSimplyConnectedBodiesInTagAction(ActionBase):
    """
    Filter simply connected bodies for tag within mesh.
        -Compute each connex bodies
        -Keep those whose measure (volume in 3D, surface en 2D) is within the interval [minRelativeMeasure*meas(support); maxRelativeMeasure*meas(support)],
        -Remove the others

    ls: level set
    minRelativeMeasure: minimum body relative measure to keep, default value 0
    maxRelativeMeasure: maximum body relative measure to keep, default value 1
    The relative measure of a body is the ratio between the measure of a simply connected body and the measure of the whole support
    """
    def __init__(self):
        super(FilterSimplyConnectedBodiesInTagAction,self).__init__()

    def Apply(self,app, ops):
        ls = ops['ls']
        originalMesh=ls.support

        tagToClean=PH.ReadString(ops.get("eTag"))
        mesh = ExtractElementByTags(originalMesh,[tagToClean])

        AddTagPerBody(mesh)
        elementsInNodalTag={nodalTag.name:ElementFilter(nTag=[nodalTag.name]) for nodalTag in mesh.nodesTags if nodalTag.name.startswith("Body_")}

        nTagsToKeep=[]
        totalMeasure=GetVolume(mesh)
        minRelativeMeasure=PH.ReadFloat(ops.get("minRelativeMeasure",0.0))
        maxRelativeMeasure=PH.ReadFloat(ops.get("maxRelativeMeasure",1.0))
        for tagName,elementFilterInTag in elementsInNodalTag.items():
            measure=GetMeasure(inmesh=mesh,elementFilter=elementFilterInTag)
            relativeMeasure=measure/totalMeasure
            Debug(tagName+": "+str(relativeMeasure))
            if minRelativeMeasure  <  relativeMeasure <  maxRelativeMeasure:
                nTagsToKeep.append(tagName)

        Debug(nTagsToKeep)
        if len(nTagsToKeep):
            elementFiltersToKeep=[elementsInTag
                                  for tagName,elementsInTag in elementsInNodalTag.items()
                                  if tagName in nTagsToKeep]
            unionElementFilter=UnionFilter(filters=elementFiltersToKeep)
            cleanMesh=ExtractElementsByElementFilter(mesh,unionElementFilter)
        else:
            Debug("No nTagsToKeep to keep: all of the "+str(tagToClean)+" portions are outside limits.")

        #Rebuild cleaned tag
        for name,data in originalMesh.elements.items():
            originalMeshElemType = originalMesh.GetElementsOfType(name)
            if tagToClean not in originalMeshElemType.tags.keys():
                continue

            if len(nTagsToKeep):
                cleanElemType = cleanMesh.GetElementsOfType(name)
                cleanExtractedMeshIds = cleanElemType.originalIds

                extractedMeshElemType = mesh.GetElementsOfType(name)
                cleanTagIdsInOriginalMesh= extractedMeshElemType.originalIds[cleanExtractedMeshIds]

                originalMeshElemType.tags[tagToClean].SetIds(cleanTagIdsInOriginalMesh)
            else:
                originalMeshElemType.tags[tagToClean].SetIds([])

        return RETURN_SUCCESS

RegisterClass("FilterSimplyConnectedBodiesInTag",FilterSimplyConnectedBodiesInTagAction)

def CheckIntegrity():
    return "ok"

if __name__ == '__main__':
    print(CheckIntegrity())
