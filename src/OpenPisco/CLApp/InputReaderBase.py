# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
import Muscat.Helpers.ParserHelper as PH

from Muscat.Helpers.IO.PathController import PathController


class InputReaderBase:
    def __init__(self):
        self.ext = ""

    def ReadFromFile(self, filename):
        data = open(filename).read()
        self.filenamePath = PathController.GetFullPathCurrentDirectoryUsingFile(filename)
        return self.ReadFromString(data)

    def ReplaceObjectFromAlmanac(self, dic, almanac):

        keysToReplace = [
            ("useLevelset", "ls"),
            ("levelset", "ls"),
            ("useProblem", "problem"),
            ("useProblems", "problems"),
            ("useOptimProblem", "optimProblem"),
            ("output", "writer"),
            ("z", "Zones"),
            ("CriteriaOffZone", "zone"),
            ("nonOptimSurfZone","zone"),
        ]
        keytomapscalar = [("grid", "grids"), ("support", "grids"), ("zone", "zones"), ("ls", "levelSets"), ("problem", "physicalProblems"), ("optimProblem", "optimProblems"), ("writer", "outputs")]

        keytomapvector = [("useProblems", "physicalProblems"), ("Zones", "zones")]

        for f, t in keysToReplace:
            if f in dic:
                dic[t] = dic[f]
                del dic[f]

        for what, where in keytomapscalar:
            if what in dic:
                try:
                    ID = PH.ReadInt(dic[what])
                except:
                    print("id must be a int not : " + str(dic[what]))
                    raise

                try:
                    dic[what] = almanac.__dict__[where][ID]
                except:
                    print("Impossible to find object in group '" + str(where) + "'  with id " + str(ID))
                    raise

        for what, where in keytomapvector:
            if what in dic:
                z = PH.ReadInts(dic[what])
                dic[what] = [almanac.__dict__[where][x] for x in z]

        if "children" in dic:
            for tag, dat in dic["children"]:
                self.ReplaceObjectFromAlmanac(dat, almanac)



def CheckIntegrity(GUI=False):
    InputReaderBase()
    return "ok"


if __name__ == "__main__":
    CheckIntegrity(GUI=True)
