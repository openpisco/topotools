# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

_test = [
"PhysicalSolverFactory",
"SolverBase",
"FieldsNames",
"GeneralZSetSolverMeca",
"StructuredFEAMecaSolver",
"UnstructuredFEAGenericSolver",
"UnstructuredFEAMecaSolver",
"UnstructuredFEAThermalSolver",
"UnstructuredFEASolverBase",
"GeneralAsterPhysicalSolver",
"AsterHarmonicForced",
"AsterModal",
"AsterStatic",
"AsterBuckling",
"AsterThermal",
"AsterThermoElastic",
"AsterSolverBase",
"PhysicalSolversTools",
"StaticFreeFemSolverMeca",
"AMInherentStrainSolver",
"AMThermalDilatationSolver",
"SolverWithSimulationDomain",
]
