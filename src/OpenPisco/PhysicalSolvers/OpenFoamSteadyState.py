# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#
from collections.abc import Iterable
from typing import Optional
import os
import typing
import shutil
from pathlib import Path

import numpy as np

from Muscat.Helpers.Logger import Info
import Muscat.Containers.ElementsDescription as ED
from Muscat.Types import MuscatFloat

from OpenPisco import RETURN_SUCCESS
import OpenPisco.PhysicalSolvers.FieldsNames as FN
from OpenPisco.PhysicalSolvers.SolverBase import SolverBase
from OpenPisco.PhysicalSolvers.PhysicalSolversTools import CellToPointData
from OpenPisco.MuscatExtentions.FoamReader import ReadExtraField
from OpenPisco.MuscatExtentions.FoamWriter import FoamWriter,CreateHeader
from OpenPisco.PhysicalSolvers.PhysicalSolversTools import ExtractInteriorMesh
from OpenPisco.ExternalTools.OpenFoam.OpenFoamInterface import OpenFoamInterface,OpenFoamGeneralConfig
import OpenPisco.TopoZones as TZ

PRESSURE = "p"
VELOCITY = "U"

class OpenFoamSteadyState(SolverBase):
    def __init__(self,openFoamConfig: Optional[OpenFoamGeneralConfig]=None):
        super(OpenFoamSteadyState,self).__init__()
        defaultParams = OpenFoamGeneralConfig()
        self.foamGeneralConfig = defaultParams.UpdateFromInstance(other=openFoamConfig)
        self.foamFormatIsBinary = self.foamGeneralConfig.writeFormat == "binary"
        self.name = "OpenFoamSteadyStateAnalysis"
        self.fluidType = "incompressibleFluid"
        self.simulationType = "laminar"
        self.ddtSchemes = "steadyState"
        self.gradSchemes = "Gauss linear"
        self.divSchemes = "bounded Gauss linearUpwind grad(U)"
        self.laplacianSchemes = "Gauss linear corrected"
        self.interpolationSchemes ="linear"
        self.snGradSchemes = "corrected"
        self.toleranceVelocity = 1e-05
        self.tolerancePressure = 1e-06
        self.algorithm = {"SIMPLE":{"nNonOrthogonalCorrectors":0,"consistent":"yes","residualControl":{PRESSURE:1e-5,VELOCITY:1e-6}}}
        self.relaxationFactors = {"equations":{VELOCITY:0.95,PRESSURE:0.95}}
        self.initials = {PRESSURE:0.0,VELOCITY:(0.0,0.0,0.0)}
        self.physicalVariables = self.initials.keys()

        self.viscosity = None
        self.dirichlet = dict()
        self.neumann   = dict()
        
        self.writer = FoamWriter
        
        fieldsICanSupply = [FN.strain,FN.stress,FN.shear_stress]
        for fieldname in fieldsICanSupply:
            self.auxiliaryFieldGeneration[fieldname][FN.Centroids] = False
            self.auxiliaryFieldGeneration[fieldname][FN.Nodes] = False         
            
        self.solverInterface = OpenFoamInterface()
                
    def SolveByLevelSet(self, levelset):
        assert levelset.conform,"Not handled for non conformal case"
        self.SetSolverInterface()
        self.DefineSolverConfig()
        self.PrepareComputationalSupport(levelset=levelset)        
        self.WriteSupport()
        self.WriteBoundaryConditions()
        self.WriteMomentumTransport()
        self.WritePhysicalProperties()
        self.WriteControlDict()
        self.WriteFVSchemes()
        self.WriteFVSolution()
        self.Solve()
        self.PostSolver()
        return RETURN_SUCCESS
    
    def GetNodalSolution(self,onCleanMesh=True):
        solutionAtCell = self.GetSolution()
        solutionAtNodes = {}
        elemtype = ED.Tetrahedron_4
        supportNumberOfNodes = self.cleanMesh.GetNumberOfNodes() if onCleanMesh else self.originalSupport.GetNumberOfNodes()
        solutionAtNodes[VELOCITY] = np.zeros((supportNumberOfNodes,3))
        for i in range(solutionAtCell[VELOCITY].shape[1]):
            velocityComponentOnCell = {elemtype:solutionAtCell[VELOCITY][:,i]} 
            velocityComponentOnPoint = CellToPointData(self.cleanMesh,velocityComponentOnCell,weightbyElementSize=False)
            solutionAtNodes[VELOCITY][:,i] = velocityComponentOnPoint

        pressure = {elemtype:solutionAtCell[PRESSURE]} 
        solutionAtNodes[PRESSURE] = CellToPointData(self.cleanMesh,pressure,weightbyElementSize=False) 
        if not onCleanMesh:
            for name in solutionAtNodes.keys():
                solutionAtNodes[name] = self.ExtendingNodalFieldToOriginalSupport(solutionAtNodes[name])           
        return solutionAtNodes
    
    def GetSolution(self):
        fieldsValues = ReadExtraField(path=self.caseFullPath,
                                      fieldNames=self.physicalVariables,
                                      fieldSupports=[ED.Tetrahedron_4]*len(self.physicalVariables),
                                      binary=self.foamFormatIsBinary)
        results = {fieldName:fieldValue for fieldName,fieldValue in zip(self.physicalVariables,fieldsValues)}
        return results

    def SetSolverInterface(self):
        self.solverInterface.SetFilename(self.name)
        uniquePath = self.GetUniqueWorkingDirectory()
        self.caseFullPath = os.path.join(uniquePath,self.name)
        self.solverInterface.SetWorkingDirectory(self.caseFullPath)

    def DefineSolverConfig(self):
        relativeTolerance = self.foamGeneralConfig.relativeTolerance
        self.solvers = {
                PRESSURE:{"solver":"PCG",
                          "preconditioner":"DIC",
                          "tolerance":self.toleranceVelocity,
                          "relTol":relativeTolerance},
                VELOCITY:{"solver":"smoothSolver",
                          "smoother":"symGaussSeidel",
                          "tolerance":self.tolerancePressure,
                          "relTol":relativeTolerance},
                          "pFinal":{"$p":None,"relTol":relativeTolerance}
                       }

    def WriteSupport(self):
        self.PrepareBoundaryConditions()    
        self.CreateCaseDirectories()
        writer = self.writer(location=os.path.join(self.caseFullPath,"constant","polyMesh"))
        writer.Write(self.cleanMesh,self.tagsToKeep)

    def PrepareBoundaryConditions(self):
        physicsFormated = {PRESSURE:{"boundary":{},"internal":self.initials[PRESSURE]},
                           VELOCITY:{"boundary":{},"internal":self.initials[VELOCITY]}}
        allTags = []
        for variable,conditions in self.dirichlet.items():
            for condition in conditions:
                value  = condition['value'] if "value" in condition else None
                if "tag" in condition:
                    singleTag = condition["tag"]
                    physicsFormated[variable]['boundary'][singleTag] = {'type': condition['type'], 'value': value} 
                    allTags.append(singleTag)
                elif "tags" in condition:
                    for singleTag in condition["tags"]:
                        physicsFormated[variable]['boundary'][singleTag] = {'type': condition['type'], 'value': value}
                        allTags.append(singleTag)
                else:
                    raise NotImplementedError("Case not handled yet!")
        
        assert len(allTags)==len(set(allTags)),"More than one boundary condition on the same tag, not allowed!"
        
        #Add zero gradient if no boundary condition
        for variable,conditions in physicsFormated.items():
            boundaryConditions = conditions["boundary"]
            for tag in allTags:
                if tag not in boundaryConditions.keys():
                    boundaryConditions[tag] = {'type': 'zeroGradient', 'value': None}

        self.boundaryConditions = physicsFormated
        self.tagsToKeep = allTags

    def CreateCaseDirectories(self):
        rootPath = self.caseFullPath
        if os.path.exists(rootPath):
            shutil.rmtree(rootPath)

        foamPath1 = os.path.join(rootPath,"constant","polyMesh")
        foamPath2 = os.path.join(rootPath,"system")
        foamPath3 = os.path.join(rootPath,"0")
        for foamPath in [foamPath1,foamPath2,foamPath3]:
            Path(foamPath).mkdir(parents=True, exist_ok=True)
        
    def WriteBoundaryConditions(self):
        self.WriteVariableConditions(variableName=PRESSURE,fieldType="volScalarField",dimensions=[0,2,-2,0,0,0,0])
        self.WriteVariableConditions(variableName=VELOCITY,fieldType="volVectorField",dimensions=[0,1,-1,0,0,0,0])

    def WriteVariableConditions(self,variableName,fieldType,dimensions):
        keyWordLocation = "0"
        variableConditions = self.boundaryConditions[variableName]
        
        filePath = os.path.join(self.caseFullPath,keyWordLocation,variableName)
        with open(filePath, "w+") as foamfile:
            header = CreateHeader(filePath=keyWordLocation,keyWordClass=fieldType,keyWordObject=variableName)
            foamfile.write(header)
            foamfile.write("\n\n")
            foamfile.write("dimensions"+"\t"+"["+" ".join([str(i) for i in dimensions])+"]"+";"+"\n\n")
            if  isinstance(variableConditions["internal"], Iterable): 
                foamfile.write("internalField   uniform"+" "+"("+" ".join([str(i) for i in variableConditions["internal"]])+")"+";"+"\n\n")
            else: 
                foamfile.write("internalField   uniform"+" "+str(variableConditions["internal"])+";"+"\n\n")
                
            foamfile.write("boundaryField"+"\n")
            foamfile.write("{"+"\n")
            for key, value in variableConditions["boundary"].items():
                foamfile.write("\t"+key+"\n")
                foamfile.write("\t"+"{"+"\n")
                inner_keys = list(value.keys())
                if value[inner_keys[1]] is None:
                    arg1= str(inner_keys[0])
                    arg2= str(value[inner_keys[0]])
                    foamfile.write("\t"+arg1+"\t"+arg2+";\n")
                else:
                    for inner_key in inner_keys:
                        arg1= str(inner_key)
                        arg2= value[inner_key]
                        if arg1 == "value":
                            if isinstance(arg2, Iterable): 
                                foamfile.write("\t"+arg1+"\t"+"uniform"+"\t"+"("+" ".join([str(i) for i in arg2])+")"+";\n")
                            else: 
                                foamfile.write("\t"+arg1+"\t"+"uniform"+"\t"+str(arg2)+";\n")
                                
                        else:
                            foamfile.write("\t"+arg1+"\t"+str(arg2)+";\n")
                foamfile.write("\t"+"}"+"\n")
            foamfile.write("}"+"\n\n")
            foamfile.write("// ************************************************************************* //")

    def WriteMomentumTransport(self):
        filename = "momentumTransport"
        keyWordLocation = "constant"
        filePath = os.path.join(self.caseFullPath,keyWordLocation,filename)
        with open(filePath, "w+") as foamfile:
            header = CreateHeader(filePath=keyWordLocation,keyWordClass="dictionary",keyWordObject=filename)
            foamfile.write(header)
            foamfile.write("\n\n")
            foamfile.write("simulationType"+"\t"+self.simulationType+";")
            foamfile.write("\n\n")
            foamfile.write("// ************************************************************************* //")
        
    def WritePhysicalProperties(self):
        filename = "physicalProperties"
        keyWordLocation = "constant"        
        filePath = os.path.join(self.caseFullPath,keyWordLocation,filename)
        header = CreateHeader(filePath=keyWordLocation,keyWordClass="dictionary",keyWordObject=filename)
        with open(filePath, "w+") as foamfile:
            foamfile.write(header)
            foamfile.write("\n\n")
            foamfile.write("viscosityModel"+"\tconstant;")
            foamfile.write("\n\n")
            foamfile.write("nu              [0 2 -1 0 0 0 0]"+" "+str(self.viscosity)+";")
            foamfile.write("\n\n")
            foamfile.write("// ************************************************************************* //")
        
    def WriteControlDict(self):
        filename = "controlDict"
        keyWordLocation = "system"
        filePath = os.path.join(self.caseFullPath,keyWordLocation,filename)
        with open(filePath, "w+") as foamfile:
            header = CreateHeader(filePath=keyWordLocation,keyWordClass="dictionary",keyWordObject=filename)
            foamfile.write(header)
            generalConfig = self.foamGeneralConfig.ToDict()
            for paramName,paramValue in generalConfig.items():
                foamfile.write("\n\n")
                foamfile.write(paramName+"\t"+str(paramValue)+";")
            foamfile.write("\n\n")
            foamfile.write("solver"+"\t"+self.fluidType+";")
            foamfile.write("\n\n")
            foamfile.write("// ************************************************************************* //")
            
    def WriteFVSchemes(self):
        filename = "fvSchemes"
        keyWordLocation = "system"        
        filePath = os.path.join(self.caseFullPath,keyWordLocation,filename)
        with open(filePath, "w+") as foamfile:
            header = CreateHeader(filePath=keyWordLocation,keyWordClass="dictionary",keyWordObject=filename)
            foamfile.write(header)
            foamfile.write("\n\n")
            solverCaracteristicsNames = ["ddtSchemes","gradSchemes","divSchemes","laplacianSchemes","interpolationSchemes","snGradSchemes"]
            solverCaracteristicsAttributes = [self.ddtSchemes,self.gradSchemes,self.divSchemes,
                                            self.laplacianSchemes,self.interpolationSchemes,self.snGradSchemes]

            for paramName,paramVal in zip(solverCaracteristicsNames,solverCaracteristicsAttributes):
                foamfile.write(paramName)
                foamfile.write("\n"+"{"+"\n")
                foamfile.write("\tdefault\t"+str(paramVal)+";\n")
                foamfile.write("}"+"\n\n")
            
            foamfile.write("// ************************************************************************* //")
        
    def WriteFVSolution(self):
        filename = "fvSolution"
        keyWordLocation = "system"
        filePath = os.path.join(self.caseFullPath,keyWordLocation,filename)
        with open(filePath, "w+") as foamfile:
            header = CreateHeader(filePath=keyWordLocation,keyWordClass="dictionary",keyWordObject=filename)
            foamfile.write(header)
            foamfile.write("\n\n")
            foamfile.write("solvers")
            foamfile.write("\n"+"{"+"\n")
            for keys in self.solvers.keys():
                foamfile.write("\t"+str(keys)+"\n")
                foamfile.write("\t"+"{"+"\n")
                for key, value in self.solvers[keys].items():
                    foamfile.write("\t\t"+str(key)+"\t"+str(value)+";\n")
                foamfile.write("\t"+"}"+"\n\n")
            foamfile.write("}"+"\n\n")
            
            algorithm = list(self.algorithm.keys())[0]
            foamfile.write(algorithm)
            foamfile.write("\n"+"{"+"\n")
            for keys, values in self.algorithm[algorithm].items():
                if isinstance(values, dict):
                    foamfile.write("\n\t"+str(keys)+"\n")
                    foamfile.write("\t"+"{"+"\n")
                    for key, value in self.algorithm[algorithm][keys].items():
                        foamfile.write("\t\t"+str(key)+"\t"+str(value)+";\n")
                    foamfile.write("\t"+"}"+"\n\n")
                else:
                    foamfile.write("\t"+str(keys)+"\t"+str(values)+";\n")

            foamfile.write("}"+"\n\n")
            foamfile.write("relaxationFactors")
            foamfile.write("\n"+"{"+"\n")
            for keys, _ in self.relaxationFactors.items():
                foamfile.write("\t"+str(keys)+"\n")
                foamfile.write("\t"+"{"+"\n")
                for key, value in self.relaxationFactors[keys].items():
                    if key[0] == "'":
                        foamfile.write("\t\t"+'"'+str(key[1:-1])+'"'+"\t"+str(value)+";\n")
                    else:
                        foamfile.write("\t\t"+str(key)+"\t"+str(value)+";\n")
                foamfile.write("\t"+"}"+"\n\n")
            foamfile.write("}"+"\n\n")
            foamfile.write("// ************************************************************************* //")

    def Solve(self):
        solverStatus = self.solverInterface.ExecuteOpenFoam()
        assert solverStatus == RETURN_SUCCESS, " OpenFoam solver has failed."

    def PostSolver(self):
        fieldRequiringVelocityGradient = [FN.strain,FN.stress,FN.shear_stress]
        auxiliaryFieldStatus = [self.auxiliaryFieldGeneration[fieldname][location] 
                                for fieldname in fieldRequiringVelocityGradient 
                                for location in [FN.Nodes,FN.Centroids]]
        if any(auxiliaryFieldStatus):
            self.solverInterface.ExecuteOpenFoamPostProcess("grad(U)")          

    def GetAuxiliaryField(self, name,on=FN.Centroids,onCleanMesh=True,index=None):
        GetFieldTypeByOn={
                      FN.Nodes:self.GetNodalField,
                      FN.Centroids:self.GetCellField,
                      }
        if self.auxiliaryFieldGeneration[name][on]:
            return GetFieldTypeByOn[on](name, onCleanMesh=onCleanMesh,index=index)
        else:
            Info("Field " +str(name)+"on "+str(on)+ "not available")
            return None        

    def GetNodalField(self,name,onCleanMesh=True,index=None):
        field = self.ComputeAuxiliaryField(name)
        if onCleanMesh:
            fieldextended = field
        else:
            fieldNbComponents = field.shape[1],field.shape[2]
            fieldextended = np.zeros( (self.originalSupport.GetNumberOfNodes(),)+fieldNbComponents,dtype=MuscatFloat)
            fieldextended[self.cleanMesh.originalIDNodes] = field
        return fieldextended

    def GetCellField(self,name,onCleanMesh=True,index=None):
        field = self.ComputeAuxiliaryField(name)
        if onCleanMesh:
            return field
        else:
            raise NotImplementedError("Can only return field on clean mesh for now")

    def ComputeAuxiliaryField(self,name):
        if name == FN.strain:
            field = self.ComputeStrain()
        elif name == FN.stress:
            field =  self.ComputeStress()
        elif name == FN.shear_stress:
            field = self.ComputeShearStress()
        else:
            raise NotImplementedError("Field not handled yet")
        return field

    def ComputeStress(self):
        shearStressField = self.ComputeShearStress()
        identity = np.broadcast_to(np.identity(3), (shearStressField.shape[0], 3, 3))
        pressure = ReadExtraField(path=self.caseFullPath,
                                  fieldNames=[PRESSURE],
                                  fieldSupports=[ED.Tetrahedron_4],
                                  binary=self.foamFormatIsBinary)[0]
        stressField = shearStressField - np.einsum('ki,kij->kij', pressure, identity)
        return stressField

    def ComputeShearStress(self):
        strainField = self.ComputeStrain()
        shearStressField =  2.0 *self.viscosity*strainField
        return shearStressField

    def ComputeStrain(self):
        gradUVector = self.GetGradientVelocity()
        gradU = gradUVector.reshape(-1,3,3)
        gradU_T = np.transpose(gradU,(0,2,1))
        strainField = (gradU +gradU_T) / 2.0
        return strainField

    def GetGradientVelocity(self):
        return ReadExtraField(path=self.caseFullPath,
                              fieldNames=["grad(U)"],
                              fieldSupports=[ED.Tetrahedron_4],
                              binary=self.foamFormatIsBinary)[0]
    
def CheckIntegrity3D(GUI:bool=False):
    from Muscat.Containers.MeshCreationTools import CreateCube
    from Muscat.ImplicitGeometry.ImplicitGeometryObjects import ImplicitGeometrySphere
    
    from OpenPisco.Unstructured.Levelset import LevelSet
    from OpenPisco.Unstructured.MmgMesher import MmgMesherActionLevelset
    
    length = np.array([8,4,4])
    refinementFactor = 1
    dimensions = refinementFactor*np.array([20, 10, 10])
    spacing = length/(dimensions-1)
    mesh = CreateCube(dimensions=dimensions,spacing=spacing,origin=[0.,0.,0.],ofTetras=True)
    tetra = mesh.GetElementsOfType(ED.Tetrahedron_4)
    tetra.tags.CreateTag(TZ.Inside3D,False).SetIds(np.arange(tetra.GetNumberOfElements()))
    ls = LevelSet(support=mesh)
    print(mesh)
    ls.conform = True
    ls.phi = np.empty(ls.support.GetNumberOfNodes())
    ls.phi.fill(-1)
    print("Computing steadystate 3D conform ...")
    
    foamConfig = OpenFoamGeneralConfig(deltaT=0.5)
    PPM = OpenFoamSteadyState(openFoamConfig=foamConfig)
    PPM.SetAuxiliaryFieldGeneration(FN.strain,on=FN.Centroids)
    PPM.SetAuxiliaryFieldGeneration(FN.stress,on=FN.Centroids)
    PPM.SetAuxiliaryFieldGeneration(FN.shear_stress,on=FN.Centroids)
    PPM.SetAuxiliaryFieldGeneration(FN.strain,on=FN.Nodes)
    PPM.SetAuxiliaryFieldGeneration(FN.stress,on=FN.Nodes)
    PPM.SetAuxiliaryFieldGeneration(FN.shear_stress,on=FN.Nodes)
    PPM.viscosity = 1e-05

    PPM.dirichlet= {PRESSURE:[{"type":"fixedValue","tag":"X1","value":0.0}],
                    VELOCITY:[{"type":"fixedValue","tag":"X0","value":(1.0,0.0,0.0)},
                     {"type":"noSlip","tags":["Y0","Y1","Z0","Z1"]}
                    ]}

    PPM.SolveByLevelSet(ls)
    assert isinstance(PPM, typing.Hashable), "OpenFoamSteadyState Should be hashable!"
    strainNodes = PPM.GetAuxiliaryField(FN.strain,on=FN.Nodes)
    stressNodes = PPM.GetAuxiliaryField(FN.stress,on=FN.Nodes)
    shearStressNodes = PPM.GetAuxiliaryField(FN.shear_stress,on=FN.Nodes)
    strainCentroid = PPM.GetAuxiliaryField(FN.strain,on=FN.Centroids)
    stressCentroid = PPM.GetAuxiliaryField(FN.stress,on=FN.Centroids)
    shearStressCentroid = PPM.GetAuxiliaryField(FN.shear_stress,on=FN.Centroids)
    solution = PPM.GetSolution()
    nodalSolution = PPM.GetNodalSolution()
    print(strainCentroid)
    print(stressCentroid)
    print(shearStressCentroid)
    print(solution)
    print(nodalSolution)
    return "ok"

def CheckIntegrity(GUI=False):
    from OpenPisco.ExternalTools.OpenFoam.OpenFoamInterface import SkipFoamTest
    skipTestPhase,message = SkipFoamTest()
    if skipTestPhase:
        return message

    from Muscat.Helpers.CheckTools import RunListOfCheckIntegrities
    return RunListOfCheckIntegrities([CheckIntegrity3D])
        
if __name__ == '__main__':
    print(CheckIntegrity())