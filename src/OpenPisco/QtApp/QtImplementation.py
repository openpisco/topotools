# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

from functools import partial


from PySide2 import QtCore, QtGui
from PySide2.QtCore import QThread
from PySide2.QtCore import Signal
from PySide2.QtCore import Slot
from PySide2.QtCore import QObject
from PySide2.QtCore import QEvent
from PySide2.QtCore import Qt
from PySide2.QtCore import QTimer
from PySide2.QtCore import QSettings
from PySide2.QtCore import QRegExp
from PySide2.QtCore import QFileInfo
from PySide2.QtCore import QFile
from PySide2.QtCore import QTextStream
from PySide2.QtCore import QSize

from PySide2.QtGui import QIcon
from PySide2.QtGui import QPixmap
from PySide2.QtGui import QColor
from PySide2.QtGui import QTextCharFormat
from PySide2.QtGui import QFont
from PySide2.QtGui import QSyntaxHighlighter
from PySide2.QtGui import QPalette

from PySide2.QtWidgets import QApplication
from PySide2.QtWidgets import QMainWindow
from PySide2.QtWidgets import QAction
from PySide2.QtWidgets import QTabWidget
from PySide2.QtWidgets import QWidget
from PySide2.QtWidgets import QGridLayout
from PySide2.QtWidgets import QComboBox
from PySide2.QtWidgets import QLabel
from PySide2.QtWidgets import QPushButton
from PySide2.QtWidgets import QSizePolicy
from PySide2.QtWidgets import QSlider
from PySide2.QtWidgets import QCheckBox
from PySide2.QtWidgets import QScrollArea
from PySide2.QtWidgets import QDockWidget
from PySide2.QtWidgets import QTextEdit
from PySide2.QtWidgets import QPlainTextEdit
from PySide2.QtWidgets import QFileDialog
from PySide2.QtWidgets import QDialog
from PySide2.QtWidgets import QInputDialog
from PySide2.QtWidgets import QLineEdit
from PySide2.QtWidgets import QFrame
from PySide2.QtWidgets import QHBoxLayout
from PySide2.QtWidgets import QVBoxLayout
from PySide2.QtWidgets import QMessageBox

from PySide2.QtOpenGL import QGLWidget

stateOn = QIcon.State.On
stateOff = QIcon.State.Off

import vtk.qt
vtk.qt.PyQtImpl = "PySide2"
from vtk.qt.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor

def GetQApplication(argv):

    if not QApplication.instance():
        return  QApplication(argv)
    else:
        return QApplication.instance()


def CheckIntegrity():
    print(QtCore.__file__)
    return 'ok'

if __name__ == '__main__':# pragma: no cover
    print(CheckIntegrity())


