# -*- coding: utf-8 -*-
#
# This file is subject to the terms and conditions defined in
# file 'LICENSE', which is part of this source code package.
#

import OpenPisco.ExternalTools.Aster.AsterCommonWriter as AsterCommonWriter 

def WriteMaterialParametersInput(writeFile,materials):
    affe_mater=[]
    info_mater={}
    for num_mater,(tagname,material) in enumerate(materials):
        info_mater['mater_'+str(num_mater)]=[]
        AsterCommonWriter.WriteVariable(writeFile,variableName='young_modulus_'+str(num_mater),variableValue=material["young"])
        AsterCommonWriter.WriteVariable(writeFile,variableName='poisson_number_'+str(num_mater),variableValue=material["poisson"])
        AsterCommonWriter.WriteVariable(writeFile,variableName='density_'+str(num_mater),variableValue=material["density"])
        amorAlpha = material["amor_alpha"] if "amor_alpha" in material.keys() else 0
        AsterCommonWriter.WriteVariable(writeFile,variableName='amor_alpha_'+str(num_mater),variableValue=amorAlpha)
        amorBeta = material["amor_beta"] if "amor_beta" in material.keys() else 0
        AsterCommonWriter.WriteVariable(writeFile,variableName='amor_beta_'+str(num_mater),variableValue=amorBeta)

        value={}
        value["E"]="young_modulus_"+str(num_mater)
        value["NU"]="poisson_number_"+str(num_mater)
        value["RHO"]="density_"+str(num_mater)
        value["AMOR_ALPHA"]="amor_alpha_"+str(num_mater)
        value["AMOR_BETA"]="amor_beta_"+str(num_mater)
        info_mater['mater_'+str(num_mater)].append(value)

        writeFile.write('mater_'+str(num_mater)+' = DEFI_MATERIAU(ELAS=_F('+','.join([ key+"="+value[key] for key in value.keys()])+'))\n')
        if tagname=='AllZones':
            affe_mater.append("_F(MATER=("+'mater_'+str(num_mater)+", ),TOUT=\'OUI\')")
            info_mater['mater_'+str(num_mater)].append("TOUT")
        else:
            affe_mater.append("_F(MATER=("+'mater_'+str(num_mater)+", ),GROUP_MA=(\""+str(tagname)+"\",))")
            info_mater['mater_'+str(num_mater)].append(tagname)
    writeFile.write('materials = {\'AFFE\' :'+str(affe_mater).replace("\'_","_").replace(")\'",")").replace("\"_","_").replace(")\"",")").replace("\"\'","\"")+',}\n')
    writeFile.write('info_mater={')
    for material in info_mater.keys():
        writeFile.write(material.replace("\'","")+":[{"+','.join("\""+param+"\""+":"+str(info_mater[material][0][param]) for param in info_mater[material][0].keys())+'},')
        writeFile.write("\""+str(info_mater[material][1]+"\"")+'],\n')
    writeFile.write('}\n')


def WriteModalBasisAnalysisParametersInput(writeFile,harmonicParams):
    min_excFreq,max_excFreq=harmonicParams['freqExcitMin'],harmonicParams['freqExcitMax']
    #Write Excitation frenquencies range
    assert min_excFreq<=max_excFreq,"freqExcitMax should be greater than freqExcitMin"
    writeFile.write("exc_interval = {'DEBUT' : "+str(min_excFreq)+", ")
    writeFile.write("'INTERVALLE':[_F(JUSQU_A="+str(max_excFreq)+", ")
    writeFile.write("PAS="+str(harmonicParams['freqExcitStep'])+")]}\n")
    #Write modal basis built instruction
    if "Numbermaxfreq" in harmonicParams.keys():
        writeFile.write("ModalBasis = {'NMAX_FREQ' : "+str(harmonicParams['Numbermaxfreq'])+"}\n")
    elif all(Parameter in harmonicParams.keys() for Parameter in ["modalBasisFreqMin","modalBasisFreqMax"]):
        min_modFreq,max_modFreq=harmonicParams['modalBasisFreqMin'],harmonicParams['modalBasisFreqMax']
        assert min_modFreq<=max_modFreq,"modalBasisFreqMax should be greater than modalBasisFreqMin"
        writeFile.write("ModalBasis = {'FREQ' : "+str((min_modFreq,max_modFreq))+"}\n")
    elif "autoModalBasisRangeFactor" in harmonicParams.keys():
        min_modFreq,max_modFreq=0.0,harmonicParams['autoModalBasisRangeFactor']*harmonicParams['freqExcitMax']
        writeFile.write("ModalBasis = {'FREQ' : "+str((min_modFreq,max_modFreq))+"}\n")
    elif "AllFreq" in harmonicParams.keys():
        writeFile.write("ModalBasis = {'ALLFREQ' : 'TOUT'}\n")
    else:
        raise Exception("No other feature available for now.")

def WritePhysicalBasisAnalysisParametersInput(writeFile,harmonicParams):
    min_excFreq,max_excFreq=harmonicParams['freqExcitMin'],harmonicParams['freqExcitMax']
    #Write Excitation frenquencies range
    assert min_excFreq<=max_excFreq,"freqExcitMax should be greater than freqExcitMin"
    writeFile.write("exc_interval = {'DEBUT' : "+str(min_excFreq)+", ")
    writeFile.write("'INTERVALLE':[_F(JUSQU_A="+str(max_excFreq)+", ")
    writeFile.write("PAS="+str(harmonicParams['freqExcitStep'])+")]}\n")


def WritePressureParametersInput(writeFile,neumann,problems):
    writeFile.write("pressures = {\n")
    for ProblemId in problems:
        if ProblemId in neumann.keys():
            for idl,_ in enumerate(neumann[ProblemId]):
                boundary=neumann[ProblemId][idl][0]
                pressureParams=neumann[ProblemId][idl][1]
                writeFile.write("'"+str(boundary)+"':{")
                for presParamsKeys,presParamsVal in pressureParams.items():
                    keyInStr="'"+str(presParamsKeys)+"'"
                    if isinstance(presParamsVal, str):
                        valInStr="'"+str(presParamsVal)+"'"
                    else:
                        valInStr=str(presParamsVal)
                    writeFile.write(keyInStr+":"+valInStr+",\n")
                writeFile.write('},\n')
    writeFile.write("}\n")

def WriteHarmonicDisplacementParametersInput(writeFile,harmonicDisp,cleanMeshNodesTags):
    writeFile.write("harmonicDisp = {'DDL_IMPO' : [\n")
    for Id in harmonicDisp:
        diristr=""
        if Id[1][0] is not None:
            diristr+="DX="+str(Id[1][0])+","
        if Id[1][1] is not None:
            diristr+="DY="+str(Id[1][1])+","
        if Id[1][2] is not None:
            diristr+="DZ="+str(Id[1][2])+","
        if Id[0] in cleanMeshNodesTags:
            group="OnNodes"
        else:
            group="OnElements"
        if group=="OnElements":
            writeFile.write("                   _F("+diristr+" GROUP_MA=('"+str(Id[0])+"', )),")
        elif group=="OnNodes":
            writeFile.write("                   _F("+diristr+" GROUP_NO=('"+str(Id[0])+"', )),")
        else:
            raise Exception("This type of group does not exist (TODO: check if GROUP_NO really required here)")
    writeFile.write("],\n")
    writeFile.write("}\n")