##########
Level Set
##########

OpenPisco provides dedicated modules to handle level sets.

A base class describing level sets can be found in the following module

* :py:meth:`OpenPisco.LevelSetBase`

A level set class is characterized by the following key attributes

* :py:attr:`OpenPisco.LevelSetBase.support` : background mesh on which the distance field is evaluated
* :py:attr:`OpenPisco.LevelSetBase.phi`     : distance field

and the following functions to handle the evolution of the implicit shape

* :py:func:`OpenPisco.LevelSetBase.Regularize` : regularization of an input velocity field
* :py:func:`OpenPisco.LevelSetBase.Transport` : transport with respect to a given velocity field and update the distance field
* :py:func:`OpenPisco.LevelSetBase.Reinitialise` : generate a distance field from an input-possibly flat- level set field

OpenPisco differentiates bewteen structured and unstructured level sets.

Structured Level Set
=======================

In the structured setting the support mesh is a structured uniform tetrahedral grid.

.. figure:: images/StructLevelset.png
    :width: 600px
    :align: center
    :alt: Structured Level Set
    :figclass: align-center

    Structured Level Set


The following module contains a derived class describing structured level sets

* :py:func:`OpenPisco.Structured.LevelSet3D`


Unstructured Level Set
=======================

In the unstructured setting the support mesh is an unstructured tetrahedral mesh.
The following module contains a derived class describing unstructured level sets

* :py:func:`OpenPisco.Unstructured.Levelset`

Utilitary functions helping the manipulation of unsturctured level sets can be found in

* :py:func:`OpenPisco.Unstructured.LevelsetTools`

Body-fitted vs non conformal level sets
----------------------------------------

OpenPisco support two modes :

* **conformal** : the background mesh is dynamically adapted to fit the 0 isovalue of the level set field
* **non conformal** : the background mesh is not adapted to the 0 isovalue of the level set field


.. figure:: images/UnstructLevelset.png
    :width: 600px
    :align: center
    :alt: Unstructured non conformal Level Set
    :figclass: align-center

    Unstructured non conformal Level Set


.. figure:: images/ConformLevelset.png
    :width: 600px
    :align: center
    :alt: Unstructured conformal Level Set
    :figclass: align-center

    Unstructured conformal Level Set


In the conformal, or body fitted, case the support mesh contains the negative part of the level set as a submesh and a triangulation of the 0 isovalue is available.
In the non conformal case the 0 isovalue is not explicitely discretized. The background mesh is fixed or adapted to the problem at stake.

The boolean attribute :py:attr:`OpenPisco.Unstructured.Levelset.conform` allows to switch from the non conformal (default) to the body-fitted mode.

Regularization
-------------------
The function :py:func:`OpenPisco.Unstructured.Levelset.Regularize` allows to extend and regularize an input velocity field defined on the structural interface in both body-fitted and non conformal settings.
The regularized velocity field is the solution :math:`V \in H^1(D)` of the following variational problem

.. math::
	\forall w\in H^{1}(D), \:\: \alpha \int_{D}{\nabla V \cdot \nabla w \:dx } + \int_{\partial\Omega}{ V w \:ds }  = \int_{\partial \Omega}{ v_{\partial \Omega} w \:ds },

where :math:`\alpha>0` is a small parameter tuning the regularization level and :math:`v_{\partial \Omega}` is the input velocity field. equation is solved using the finite elements framework of the library Muscat [1]_.


Transport
-------------------
The function :py:func:`OpenPisco.Unstructured.Levelset.Transport` allows to transport an initial level set field along a given velocity field by solving the following level set transport equation

.. math::
    \frac{\partial \phi}{\partial t } + \theta \cdot \nabla \phi  = \frac{\partial \phi}{\partial t } -v \lvert \nabla \phi \lvert   =  0 \text{ in } D.

where :math:`\theta = -v n` is a normal velocity field. The transport equation is numerically solved using the class :py:class:`OpenPisco.Unstructured.Levelset.Advect`. This class implements a python interface to the open source library Advection [2]_.


Reinitialization
-------------------
The redistanciation equation is numerically solved using the class :py:class:`OpenPisco.Unstructured.Meshdist`. This class implements a python interface to the open source library Mshdist [3]_.



.. [1] https://gitlab.com/drti/muscat

.. [2] https://github.com/ISCDtoolbox/Advection

.. [3] https://github.com/ISCDtoolbox/Mshdist
